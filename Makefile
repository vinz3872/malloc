# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/04/26 10:37:26 by vjacquie          #+#    #+#              #
#    Updated: 2016/04/27 13:15:00 by vjacquie         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

HOSTTYPE := $(HOSTTYPE)
ifndef HOSTTYPE
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif
NAME = libft_malloc_$(HOSTTYPE).so

CC = gcc
CFLAGS = -Wall -Werror -Wextra
# CFLAGS = -Wall -Werror -Wextra -std=gnu89 -pedantic -o3

HEADER = includes/ft_malloc.h
INCLUDE = -I libft/includes/ -I includes/

LIB_MALLOC = libft_malloc.so

LIB = libft/libft.a

SRCS =	src/ft_malloc.c \
		src/ft_free.c \
		src/ft_realloc.c \
		src/get_free_large_zone.c \
		src/get_free_small_zone.c \
		src/get_free_tiny_zone.c \
		src/get_ptr.c \
		src/init_mem.c \
		src/print_hex.c \
		src/show_alloc_mem.c \
		src/get_size.c

OBJS = $(SRCS:.c=.o)

all: $(NAME) $(LIB_MALLOC)

$(LIB):
	make -C libft

%.o: %.c $(HEADER)
	@$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@

$(NAME): $(LIB) $(OBJS) $(HEADER)
	$(CC) $(CFLAGS) -shared $(INCLUDE) $(OBJS) $(LIB) -o $(NAME)

$(LIB_MALLOC) : $(NAME)
	ln -fs $(NAME) $(LIB_MALLOC)

clean:
	make clean -C libft
	rm -rf $(OBJS)

fclean: clean
	make fclean -C libft
	rm -rf $(NAME)
	rm -rf $(LIB_MALLOC)

re: fclean all

.PHONY: all clean fclean re
