/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 11:05:38 by vjacquie          #+#    #+#             */
/*   Updated: 2016/04/27 13:13:27 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MALLOC_H
# define FT_MALLOC_H

# define GETPAGESIZE	getpagesize()
# include "../libft/includes/libft.h"
# include <sys/mman.h>

enum				e_type {
	FREE,
	USED,
	SMALL,
	TINY,
	LARGE
};

typedef struct		s_log {
	void			*ptr;
	int				type;
	int				status;
	size_t			used;
}					t_log;

typedef struct		s_zone {
	void			*ptr;
	t_log			log_zone[100];
	char			*content;
	int				type;
	int				used;
	struct s_zone	*next;
}					t_zone;

void				show_alloc_mem();

void				print_hex(unsigned long n);

int					init_mem();
void				add_zone(t_zone *lst, t_zone *new);
t_zone				*create_zone(int type);
int					init_content_large_zone(t_zone *zone, int type);
int					init_content_zone(t_zone *zone, int type);

size_t				get_size(int type);

t_zone				*get_tiny_ptr();
t_zone				*get_small_ptr();
t_zone				*get_large_ptr();

void				*get_tiny_free_zone(size_t size);
void				*get_tiny_free_zone_ptr(t_zone *tmp, size_t size);

void				*get_small_free_zone(size_t size);
void				*get_small_free_zone_ptr(t_zone *tmp, size_t size);

void				*get_large_free_zone(size_t size);
void				*get_large_free_zone_ptr(t_zone *tmp, size_t size);

void				*realloc(void *ptr, size_t size);

void				*malloc(size_t size);

void				free(void *ptr);

extern void			*malloc(size_t size);
extern void			free(void *ptr);
extern void			*realloc(void *ptr, size_t size);
extern void			show_alloc_mem();

#endif
