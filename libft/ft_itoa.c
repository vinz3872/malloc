/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 14:55:05 by vjacquie          #+#    #+#             */
/*   Updated: 2014/04/24 15:33:29 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

static char	*ft_itoa2(int mod, char *str, long cpy, int n)
{
	int		power;
	int		i;

	power = 1;
	i = 0;
	while (mod != 1)
	{
		power = (power * 10);
		mod--;
	}
	if (n < 0)
	{
		str[i] = '-';
		i++;
	}
	while (power != 0)
	{
		str[i] = ('0' + ((cpy / power) % 10));
		power = (power / 10);
		i++;
	}
	return (str);
}

static long	ft_test(int n)
{
	long	cpy;

	cpy = n;
	if (n < 0)
	{
		cpy = (cpy * (-1));
	}
	return (cpy);
}

char		*ft_itoa(int n)
{
	int		mod;
	long	cpy;
	char	*str;
	long	tps;

	cpy = ft_test(n);
	tps = cpy;
	mod = 0;
	while (cpy != 0)
	{
		cpy = cpy / 10;
		mod++;
	}
	str = (char *)malloc(sizeof(char) * (mod + 2));
	ft_bzero(str, (mod + 2));
	if (str == NULL)
		return (NULL);
	cpy = tps;
	if (cpy == 0)
	{
		str[0] = '0';
		return (str);
	}
	ft_itoa2(mod, str, cpy, n);
	return (str);
}
