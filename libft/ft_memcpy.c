/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 12:51:31 by vjacquie          #+#    #+#             */
/*   Updated: 2014/04/24 15:33:39 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

void	*ft_memcpy(void *s1, const void *s2, size_t n)
{
	size_t		i;
	char		*temps;
	char		*dest;
	char		*src;

	i = 0;
	temps = (char *)malloc(sizeof(char) * n);
	dest = (char *)s2;
	src = (char *)s1;
	while (i != n)
	{
		temps[i] = dest[i];
		i = i + 1;
	}
	i = 0;
	while (i != n)
	{
		src[i] = temps[i];
		i = i + 1;
	}
	free(temps);
	return (s1);
}
