/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 13:07:06 by vjacquie          #+#    #+#             */
/*   Updated: 2014/04/24 15:33:55 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

void	ft_putendl_fd(char const *s, int fd)
{
	char	*s1;
	int		i;

	s1 = (char *)s;
	i = 0;
	while (s1[i])
	{
		write(fd, &s1[i], 1);
		i++;
	}
	write(fd, "\n", 1);
}
