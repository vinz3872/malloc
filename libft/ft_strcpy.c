/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 12:51:31 by vjacquie          #+#    #+#             */
/*   Updated: 2014/04/24 15:34:14 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

char	*ft_strcpy(char *s1, const char *s2)
{
	int	i;
	int	j;

	j = 0;
	i = ft_strlen(s2);
	while (j != i)
	{
		s1[j] = s2[j];
		j = j + 1;
	}
	s1[j] = '\0';
	return (s1);
}
