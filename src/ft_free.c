/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 13:09:54 by vjacquie          #+#    #+#             */
/*   Updated: 2016/04/27 11:04:04 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_malloc.h>

static void	free_large(void *ptr)
{
	t_zone	*tmp;
	int		i;

	tmp = get_large_ptr();
	i = 0;
	while (tmp != NULL && i < 100)
	{
		if (tmp->log_zone[i].status == USED && tmp->log_zone[i].ptr == ptr)
		{
			if (munmap(tmp->log_zone[i].ptr, tmp->log_zone[i].used) < 0)
				ft_putendl("munmap failed");
			else
			{
				tmp->log_zone[i].status = FREE;
				tmp->log_zone[i].used = 0;
				tmp->used--;
			}
			return ;
		}
		i++;
		if (i >= 100)
			i = 0;
		if (i == 0)
			tmp = tmp->next;
	}
}

static void	free_small(void *ptr)
{
	t_zone	*tmp;
	int		i;

	tmp = get_small_ptr();
	i = 0;
	while (tmp != NULL && i < 100)
	{
		if (tmp->log_zone[i].status == USED && tmp->log_zone[i].ptr == ptr)
		{
			tmp->log_zone[i].status = FREE;
			tmp->log_zone[i].used = 0;
			tmp->used--;
			return ;
		}
		i++;
		if (i >= 100)
		{
			i = 0;
			tmp = tmp->next;
		}
	}
	free_large(ptr);
}

static void	free_tiny(void *ptr)
{
	t_zone	*tmp;
	int		i;

	tmp = get_tiny_ptr();
	i = 0;
	while (tmp != NULL && i < 100)
	{
		if (tmp->log_zone[i].status == USED && tmp->log_zone[i].ptr == ptr)
		{
			tmp->log_zone[i].status = FREE;
			tmp->log_zone[i].used = 0;
			tmp->used--;
			return ;
		}
		i++;
		if (i >= 100)
		{
			i = 0;
			tmp = tmp->next;
		}
	}
	free_small(ptr);
}

void		free(void *ptr)
{
	if (ptr == NULL)
		return ;
	free_tiny(ptr);
}
