/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 10:46:18 by vjacquie          #+#    #+#             */
/*   Updated: 2016/04/26 12:18:45 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_malloc.h>

void	*malloc(size_t size)
{
	if (init_mem() < 0 || size == 0)
		return (NULL);
	if (size <= get_size(TINY))
		return (get_tiny_free_zone(size));
	else if (size > get_size(TINY) && size <= get_size(SMALL))
		return (get_small_free_zone(size));
	else if (size > get_size(SMALL))
		return (get_large_free_zone(size));
	return (NULL);
}
