/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 13:35:11 by vjacquie          #+#    #+#             */
/*   Updated: 2016/04/27 11:02:57 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_malloc.h>

static size_t	search_ptr_size(t_zone *tmp, void *ptr)
{
	int			i;

	i = 0;
	while (tmp != NULL && i < 100)
	{
		if (tmp->log_zone[i].status == USED && tmp->log_zone[i].ptr == ptr)
			return (tmp->log_zone[i].used);
		i++;
		if (i >= 100)
		{
			i = 0;
			tmp = tmp->next;
		}
	}
	return (0);
}

static void		*do_realloc(size_t size, void *ptr)
{
	void		*new;

	if ((new = malloc(size)) == NULL)
		return (NULL);
	new = ft_memcpy(new, ptr, sizeof(unsigned char) * size);
	free(ptr);
	return (new);
}

void			*realloc(void *ptr, size_t size)
{
	t_zone		*tmp;
	size_t		len;

	if (ptr == NULL)
		return (malloc(size));
	else if (size <= 0)
	{
		free(ptr);
		return (NULL);
	}
	tmp = get_tiny_ptr();
	len = search_ptr_size(tmp, ptr);
	if (len == 0)
	{
		tmp = get_small_ptr();
		len = search_ptr_size(tmp, ptr);
	}
	if (len == 0)
	{
		tmp = get_large_ptr();
		len = search_ptr_size(tmp, ptr);
	}
	if (len != 0)
		return (do_realloc(size, ptr));
	return (NULL);
}
