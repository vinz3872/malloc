/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_free_large_zone.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 13:01:06 by vjacquie          #+#    #+#             */
/*   Updated: 2016/04/27 11:02:33 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_malloc.h>

void	*get_large_free_zone(size_t size)
{
	t_zone	*tmp;
	t_zone	*new;

	if ((tmp = get_large_ptr()) == NULL)
		return (NULL);
	while (tmp != NULL)
	{
		if (tmp->used < 100)
			return (get_large_free_zone_ptr(tmp, size));
		tmp = tmp->next;
	}
	if ((tmp = get_large_ptr()) == NULL
		|| (new = create_zone(LARGE)) == NULL)
		return (NULL);
	add_zone(tmp, new);
	return (get_large_free_zone_ptr(new, size));
}

void	*get_large_free_zone_ptr(t_zone *tmp, size_t size)
{
	int		i;

	i = 0;
	while (i < 100)
	{
		if (tmp->log_zone[i].status == FREE)
		{
			tmp->log_zone[i].ptr = mmap(0, size, PROT_READ | PROT_WRITE, \
					MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
			if (tmp->log_zone[i].ptr == MAP_FAILED)
				return (NULL);
			tmp->log_zone[i].status = USED;
			tmp->log_zone[i].used = size;
			tmp->used++;
			return ((void *)tmp->log_zone[i].ptr);
		}
		i++;
	}
	return (NULL);
}
