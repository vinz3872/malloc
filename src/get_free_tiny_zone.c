/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_free_tiny_zone.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 12:59:36 by vjacquie          #+#    #+#             */
/*   Updated: 2016/04/27 11:01:45 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_malloc.h>

void	*get_tiny_free_zone(size_t size)
{
	t_zone	*tmp;
	t_zone	*new;

	if ((tmp = get_tiny_ptr()) == NULL)
		return (NULL);
	while (tmp != NULL)
	{
		if (tmp->used < 100)
			return (get_tiny_free_zone_ptr(tmp, size));
		tmp = tmp->next;
	}
	if ((tmp = get_tiny_ptr()) == NULL
		|| (new = create_zone(TINY)) == NULL)
		return (NULL);
	add_zone(tmp, new);
	return (get_tiny_free_zone_ptr(new, size));
}

void	*get_tiny_free_zone_ptr(t_zone *tmp, size_t size)
{
	int		i;

	i = 0;
	while (i < 100)
	{
		if (tmp->log_zone[i].status == FREE)
		{
			tmp->log_zone[i].status = USED;
			tmp->log_zone[i].used = size;
			tmp->used++;
			return ((void *)tmp->log_zone[i].ptr);
		}
		i++;
	}
	return (NULL);
}
