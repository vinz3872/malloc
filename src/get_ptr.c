/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_ptr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 12:07:50 by vjacquie          #+#    #+#             */
/*   Updated: 2016/04/26 12:10:51 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_malloc.h>

t_zone		*get_tiny_ptr(void)
{
	static t_zone	*list_tiny = NULL;

	if (list_tiny == NULL)
	{
		list_tiny = create_zone(TINY);
		if (list_tiny == NULL)
			return (NULL);
	}
	return (list_tiny);
}

t_zone		*get_small_ptr(void)
{
	static t_zone	*list_small = NULL;

	if (list_small == NULL)
	{
		list_small = create_zone(SMALL);
		if (list_small == NULL)
			return (NULL);
	}
	return (list_small);
}

t_zone		*get_large_ptr(void)
{
	static t_zone	*list_large = NULL;

	if (list_large == NULL)
	{
		list_large = create_zone(LARGE);
		if (list_large == NULL)
			return (NULL);
	}
	return (list_large);
}
