/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_size.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 13:10:59 by vjacquie          #+#    #+#             */
/*   Updated: 2016/04/27 13:11:10 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_malloc.h>

size_t	get_size(int type)
{
	if (type == SMALL)
		return ((GETPAGESIZE / 8));
	else if (type == TINY)
		return ((GETPAGESIZE / 4));
	else
		return ((GETPAGESIZE));
}
