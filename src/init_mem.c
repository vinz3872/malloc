/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_mem.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 11:08:35 by vjacquie          #+#    #+#             */
/*   Updated: 2016/04/27 13:11:47 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_malloc.h>

int		init_content_zone(t_zone *zone, int type)
{
	int	i;

	zone->content = (char *)mmap(0, (get_size(type) * 100), \
			PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (zone->content == MAP_FAILED)
		return (-1);
	zone->ptr = zone;
	i = 0;
	while (i < 100)
	{
		zone->log_zone[i].ptr = &zone->content[i * get_size(type)];
		zone->log_zone[i].type = type;
		zone->log_zone[i].status = FREE;
		zone->log_zone[i].used = 0;
		i++;
	}
	return (0);
}

int		init_content_large_zone(t_zone *zone, int type)
{
	int	i;

	zone->content = NULL;
	zone->ptr = zone;
	i = 0;
	while (i < 100)
	{
		zone->log_zone[i].ptr = NULL;
		zone->log_zone[i].type = type;
		zone->log_zone[i].status = FREE;
		zone->log_zone[i].used = 0;
		i++;
	}
	return (0);
}

t_zone	*create_zone(int type)
{
	t_zone *new;

	new = (t_zone *)mmap(0, sizeof(t_log), PROT_READ | PROT_WRITE, \
			MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (new == MAP_FAILED)
		return (NULL);
	new->type = type;
	new->used = 0;
	new->next = NULL;
	if (type != LARGE && init_content_zone(new, type) < 0)
		return (NULL);
	else if (type == LARGE && init_content_large_zone(new, type) < 0)
		return (NULL);
	return (new);
}

void	add_zone(t_zone *lst, t_zone *new)
{
	t_zone *tmp;

	tmp = lst;
	while (tmp->next != NULL)
		tmp = tmp->next;
	tmp->next = new;
}

int		init_mem(void)
{
	if (get_tiny_ptr() == NULL || get_small_ptr() == NULL
		|| get_large_ptr() == NULL)
		return (-1);
	return (0);
}
