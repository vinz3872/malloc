/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_hex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 10:51:47 by vjacquie          #+#    #+#             */
/*   Updated: 2016/04/27 10:52:13 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_malloc.h>

void	print_hex(unsigned long n)
{
	if (n >= 16)
		print_hex(n / 16);
	if ((n % 16) < 10)
		ft_putchar((n % 16) + '0');
	else
	{
		if ((n % 16) == 10)
			ft_putchar('a');
		else if ((n % 16) == 11)
			ft_putchar('b');
		else if ((n % 16) == 12)
			ft_putchar('c');
		else if ((n % 16) == 13)
			ft_putchar('d');
		else if ((n % 16) == 14)
			ft_putchar('e');
		else if ((n % 16) == 15)
			ft_putchar('f');
	}
}
