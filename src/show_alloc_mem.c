/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 10:29:41 by vjacquie          #+#    #+#             */
/*   Updated: 2016/04/27 10:58:58 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_malloc.h>

static void	print_mem(t_log data)
{
	ft_putstr("0x");
	print_hex((unsigned long)data.ptr);
	ft_putstr(" - ");
	ft_putstr("0x");
	print_hex((unsigned long)&data.ptr[data.used]);
	ft_putstr(" : ");
	ft_putnbr(data.used);
	ft_putendl(" octets");
}

static void	print_ptr_mem(void *ptr, int type)
{
	if (type == TINY)
		ft_putstr("TINY : 0x");
	else if (type == SMALL)
		ft_putstr("SMALL : 0x");
	else
		ft_putstr("LARGE : 0x");
	print_hex((unsigned long)ptr);
	ft_putchar('\n');
}

static int	get_list_size(t_zone *tmp, int type)
{
	int		i;
	int		count;

	i = 0;
	count = 0;
	while (tmp != NULL && i < 100)
	{
		if (i == 0)
			print_ptr_mem(tmp->ptr, type);
		if (tmp->log_zone[i].status == USED)
		{
			count += tmp->log_zone[i].used;
			print_mem(tmp->log_zone[i]);
		}
		i++;
		if (i >= 100)
			i = 0;
		if (i == 0)
			tmp = tmp->next;
	}
	return (count);
}

void		show_alloc_mem(void)
{
	int		total;
	t_zone	*tmp;

	tmp = get_tiny_ptr();
	total = get_list_size(tmp, TINY);
	tmp = get_small_ptr();
	total += get_list_size(tmp, SMALL);
	tmp = get_large_ptr();
	total += get_list_size(tmp, LARGE);
	ft_putstr("Total :");
	ft_putnbr(total);
	ft_putendl(" octets");
}
